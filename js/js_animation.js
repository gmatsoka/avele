window.addEventListener("DOMContentLoaded", function() {
  var button = document.getElementById("navbardrop");
  var menu = document.getElementById("navbarmenu");
  button.addEventListener("click", function(event) {
    menu.classList.toggle("opened");
  });
});

function openstep1(start, element) {
  return function(timestamp) {
    if (!start){
      start = timestamp;
    }
    var progress = timestamp - start;
    element.style.left = Math.min(30 + (progress) / 10, 35) + "vw";
    if (progress < 1000) {
      window.requestAnimationFrame(openstep1(start, element));
    }
  }
}

function openstep2(start, element) {
  return function(timestamp) {
    if (!start){
      start = timestamp;
    }
    var progress = timestamp - start;
    element.style.top = Math.max(100 - (progress)/5, 15) + "vh";
    if (progress < 1000) {
      window.requestAnimationFrame(openstep2(start, element));
    }
  }
}

function openstep3(start, element) {
  return function(timestamp) {
    if (!start){
      start = timestamp;
    }
    var progress = timestamp - start;
    element.style.width = Math.min(progress + 0, 300) + "px";
    element.style.height = Math.min(progress + 0, 400) + "px";
    if (progress < 1000) {
      window.requestAnimationFrame(openstep3(start, element));
    }
  }
}

function closestep1(start, element) {
  return function(timestamp) {
    if (!start){
      start = timestamp;
    }
    var progress = timestamp - start;
    element.style.left = Math.max(35 + (progress) / 10, 35) + "vw";
    if (progress < 1000) {
      window.requestAnimationFrame(closestep1(start, element));
    }
  }
}

function closestep2(start, element) {
  return function(timestamp) {
    if (!start){
      start = timestamp;
    }
    var progress = timestamp - start;
    element.style.top = Math.min(15 - (progress)/5, 15) + "vh";
    if (progress < 1000) {
      window.requestAnimationFrame(closestep2(start, element));
    }
  }
}

function closestep3(start, element) {
  return function(timestamp) {
    if (!start){
      start = timestamp;
    }
    var progress = timestamp - start;
    element.style.width = Math.max(300 - progress, 300) + "px";
    element.style.height = Math.max(400 - progress, 400) + "px";
    if (progress < 1000) {
      window.requestAnimationFrame(closestep3(start, element));
    }
  }
}

$("document").ready(function(){
  $(".knopochka").on("click", function(){
    var start = null;
    var element = document.getElementById("form_popup");
    element.style.position = "fixed";
    element.style.width = "0px";
    element.style.height = "0px";
    document.body.appendChild(element);
    window.requestAnimationFrame(openstep1(start, element));
    window.requestAnimationFrame(openstep2(start, element));
    window.requestAnimationFrame(openstep3(start, element));
  });
  $(".close").click(function(){
    var start = null;
    var element = document.getElementById("form_popup");
    element.style.position = "fixed";
    element.style.width = "0px";
    element.style.height = "0px";
    document.body.appendChild(element);
    window.requestAnimationFrame(closestep1(start, element));
    window.requestAnimationFrame(closestep2(start, element));
    window.requestAnimationFrame(closestep3(start, element));
  });
});
