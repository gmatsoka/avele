function Sim(sldrId) {

	let id = document.getElementById(sldrId);
	if(id) {
		this.sldrRoot = id
	}
	else {
		this.sldrRoot = document.querySelector('.sim-slider3')
	};

	// Carousel objects
	this.sldrList = this.sldrRoot.querySelector('.sim-slider-list3');
	this.sldrElements = this.sldrList.querySelectorAll('.sim-slider-element3');
	this.sldrElemFirst = this.sldrList.querySelector('.sim-slider-element3');
	this.leftArrow = this.sldrRoot.querySelector('div.sim-slider-arrow-left3');
	this.rightArrow = this.sldrRoot.querySelector('div.sim-slider-arrow-right3');
	this.indicatorDots = this.sldrRoot.querySelector('div.sim-slider-dots3');

	// Initialization
	this.options = Sim.defaults;
	Sim.initialize(this)
};

Sim.defaults = {

	// Default options for the carousel
	loop: true,     // ÐÐµÑÐºÐ¾Ð½ÐµÑÐ½Ð¾Ðµ Ð·Ð°ÑÐ¸ÐºÐ»Ð¸Ð²Ð°Ð½Ð¸Ðµ ÑÐ»Ð°Ð¹Ð´ÐµÑÐ°
	auto: true,     // ÐÐ²ÑÐ¾Ð¼Ð°ÑÐ¸ÑÐµÑÐºÐ¾Ðµ Ð¿ÑÐ¾Ð»Ð¸ÑÑÑÐ²Ð°Ð½Ð¸Ðµ
	interval: 5000, // ÐÐ½ÑÐµÑÐ²Ð°Ð» Ð¼ÐµÐ¶Ð´Ñ Ð¿ÑÐ¾Ð»Ð¸ÑÑÑÐ²Ð°Ð½Ð¸ÐµÐ¼ ÑÐ»ÐµÐ¼ÐµÐ½ÑÐ¾Ð² (Ð¼Ñ)
	arrows: true,   // ÐÑÐ¾Ð»Ð¸ÑÑÑÐ²Ð°Ð½Ð¸Ðµ ÑÑÑÐµÐ»ÐºÐ°Ð¼Ð¸
	dots: true      // ÐÐ½Ð´Ð¸ÐºÐ°ÑÐ¾ÑÐ½ÑÐµ ÑÐ¾ÑÐºÐ¸
};

Sim.prototype.elemPrev = function(num) {
	num = num || 1;

	let prevElement = this.currentElement;
	this.currentElement -= num;
	if(this.currentElement < 0) this.currentElement = this.elemCount-1;

	if(!this.options.loop) {
		if(this.currentElement == 0) {
			this.leftArrow.style.display = 'none'
		};
		this.rightArrow.style.display = 'block'
	};
	
	this.sldrElements[this.currentElement].style.opacity = '1';
	this.sldrElements[prevElement].style.opacity = '0';

	if(this.options.dots) {
		this.dotOn(prevElement); this.dotOff(this.currentElement)
	}
};

Sim.prototype.elemNext = function(num) {
	num = num || 1;
	
	let prevElement = this.currentElement;
	this.currentElement += num;
	if(this.currentElement >= this.elemCount) this.currentElement = 0;

	if(!this.options.loop) {
		if(this.currentElement == this.elemCount-1) {
			this.rightArrow.style.display = 'none'
		};
		this.leftArrow.style.display = 'block'
	};

	this.sldrElements[this.currentElement].style.opacity = '1';
	this.sldrElements[prevElement].style.opacity = '0';

	if(this.options.dots) {
		this.dotOn(prevElement); this.dotOff(this.currentElement)
	}
};

Sim.prototype.dotOn = function(num) {
	this.indicatorDotsAll[num].style.cssText = 'background-color:#BBB; cursor:pointer;'
};

Sim.prototype.dotOff = function(num) {
	this.indicatorDotsAll[num].style.cssText = 'background-color:#556; cursor:default;'
};

Sim.initialize = function(that) {

	// Constants
	that.elemCount = that.sldrElements.length; // ÐÐ¾Ð»Ð¸ÑÐµÑÑÐ²Ð¾ ÑÐ»ÐµÐ¼ÐµÐ½ÑÐ¾Ð²

	// Variables
	that.currentElement = 0;
	let bgTime = getTime();

	// Functions
	function getTime() {
		return new Date().getTime();
	};
	function setAutoScroll() {
		that.autoScroll = setInterval(function() {
			let fnTime = getTime();
			if(fnTime - bgTime + 10 > that.options.interval) {
				bgTime = fnTime; that.elemNext()
			}
		}, that.options.interval)
	};

	// Start initialization
	if(that.elemCount <= 1) {   // ÐÑÐºÐ»ÑÑÐ¸ÑÑ Ð½Ð°Ð²Ð¸Ð³Ð°ÑÐ¸Ñ
		that.options.auto = false; that.options.arrows = false; that.options.dots = false;
		that.leftArrow.style.display = 'none'; that.rightArrow.style.display = 'none'
	};
	if(that.elemCount >= 1) {   // Ð¿Ð¾ÐºÐ°Ð·Ð°ÑÑ Ð¿ÐµÑÐ²ÑÐ¹ ÑÐ»ÐµÐ¼ÐµÐ½Ñ
		that.sldrElemFirst.style.opacity = '1';
	};

	if(!that.options.loop) {
		that.leftArrow.style.display = 'none';  // Ð¾ÑÐºÐ»ÑÑÐ¸ÑÑ Ð»ÐµÐ²ÑÑ ÑÑÑÐµÐ»ÐºÑ
		that.options.auto = false; // Ð¾ÑÐºÐ»ÑÑÐ¸ÑÑ Ð°Ð²ÑÐ¾Ð¿ÑÐºÑÑÑÐºÑ
	}
	else if(that.options.auto) {   // Ð¸Ð½Ð¸ÑÐ¸Ð°Ð»Ð¸Ð·Ð°ÑÐ¸Ñ Ð°Ð²ÑÐ¾Ð¿ÑÐ¾ÐºÑÑÐºÐ¸
		setAutoScroll();
		// ÐÑÑÐ°Ð½Ð¾Ð²ÐºÐ° Ð¿ÑÐ¾ÐºÑÑÑÐºÐ¸ Ð¿ÑÐ¸ Ð½Ð°Ð²ÐµÐ´ÐµÐ½Ð¸Ð¸ Ð¼ÑÑÐ¸ Ð½Ð° ÑÐ»ÐµÐ¼ÐµÐ½Ñ
		that.sldrList.addEventListener('mouseenter', function() {clearInterval(that.autoScroll)}, false);
		that.sldrList.addEventListener('mouseleave', setAutoScroll, false)
	};

	if(that.options.arrows) {  // Ð¸Ð½Ð¸ÑÐ¸Ð°Ð»Ð¸Ð·Ð°ÑÐ¸Ñ ÑÑÑÐµÐ»Ð¾Ðº
		that.leftArrow.addEventListener('click', function() {
			let fnTime = getTime();
			if(fnTime - bgTime > 1000) {
				bgTime = fnTime; that.elemPrev()
			}
		}, false);
		that.rightArrow.addEventListener('click', function() {
			let fnTime = getTime();
			if(fnTime - bgTime > 1000) {
				bgTime = fnTime; that.elemNext()
			}
		}, false)
	}
	else {
		that.leftArrow.style.display = 'none'; that.rightArrow.style.display = 'none'
	};

	if(that.options.dots) {  // Ð¸Ð½Ð¸ÑÐ¸Ð°Ð»Ð¸Ð·Ð°ÑÐ¸Ñ Ð¸Ð½Ð´Ð¸ÐºÐ°ÑÐ¾ÑÐ½ÑÑ ÑÐ¾ÑÐµÐº
		let sum = '', diffNum;
		for(let i=0; i<that.elemCount; i++) {
			sum += '<span class="sim-dot"></span>'
		};
		that.indicatorDots.innerHTML = sum;
		that.indicatorDotsAll = that.sldrRoot.querySelectorAll('span.sim-dot');
		// ÐÐ°Ð·Ð½Ð°ÑÐ°ÐµÐ¼ ÑÐ¾ÑÐºÐ°Ð¼ Ð¾Ð±ÑÐ°Ð±Ð¾ÑÑÐ¸Ðº ÑÐ¾Ð±ÑÑÐ¸Ñ 'click'
		for(let n=0; n<that.elemCount; n++) {
			that.indicatorDotsAll[n].addEventListener('click', function() {
				diffNum = Math.abs(n - that.currentElement);
				if(n < that.currentElement) {
					bgTime = getTime(); that.elemPrev(diffNum)
				}
				else if(n > that.currentElement) {
					bgTime = getTime(); that.elemNext(diffNum)
				}
				// ÐÑÐ»Ð¸ n == that.currentElement Ð½Ð¸ÑÐµÐ³Ð¾ Ð½Ðµ Ð´ÐµÐ»Ð°ÐµÐ¼
			}, false)
		};
		that.dotOff(0);  // ÑÐ¾ÑÐºÐ°[0] Ð²ÑÐºÐ»ÑÑÐµÐ½Ð°, Ð¾ÑÑÐ°Ð»ÑÐ½ÑÐµ Ð²ÐºÐ»ÑÑÐµÐ½Ñ
		for(let i=1; i<that.elemCount; i++) {
			that.dotOn(i)
		}
	}
};

new Sim();